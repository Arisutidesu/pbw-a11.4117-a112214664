 <!-- ======= Portfolio Section ======= -->
 <section id="gambar" class="portfolio">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Gambar Jamu</h2>
          <p>Gambar-gambar dibawah ini adalah gambar jamu yang saya dapat melalui interne yang berasal dari Wikipedia, Google, dan Website dan sebenarnya masih ada banyak gambar jamu yang tidak tercantum dibawah</p>
        </div>

        <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
          <li data-filter="*" class="filter-active">All</li>
          <li data-filter=".filter-wik">Wikipedia</li>
          <li data-filter=".filter-goo">Google</li>
          <li data-filter=".filter-web">Website</li>
        </ul>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

          <div class="col-lg-4 col-md-6 portfolio-item filter-wik">
            <div class="portfolio-img"><img src="assets/img/portfolio/sawanan.jpg" class="img-fluid" alt=""></div>
            <div class="portfolio-info">
              <h4>Wikipedia</h4>
              <p>Jamu Sawanan</p>
              <a href="assets/img/portfolio/sawanan.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"><i class="bx bx-plus"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-img"><img src="assets/img/portfolio/pahitan.jpg" class="img-fluid" alt=""></div>
            <div class="portfolio-info">
              <h4>Website</h4>
              <p>Jamu Pahitan Brotowali</p>
              <a href="assets/img/portfolio/pahitan.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"><i class="bx bx-plus"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-goo">
            <div class="portfolio-img"><img src="assets/img/portfolio/kunyitasam.jpg" class="img-fluid" alt=""></div>
            <div class="portfolio-info">
              <h4>Google</h4>
              <p>Jamu Kunyit Asam</p>
              <a href="assets/img/portfolio/kunyitasam.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"><i class="bx bx-plus"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-goo">
            <div class="portfolio-img"><img src="assets/img/portfolio/temulawak.jpg" class="img-fluid" alt=""></div>
            <div class="portfolio-info">
              <h4>Google</h4>
              <p>Jamu Temulawak</p>
              <a href="assets/img/portfolio/temulawak.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"><i class="bx bx-plus"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-goo">
            <div class="portfolio-img"><img src="assets/img/portfolio/beraskencur.jpg" class="img-fluid" alt=""></div>
            <div class="portfolio-info">
              <h4>Google</h4>
              <p>Jamu Beras Kencur</p>
              <a href="assets/img/portfolio/beraskencur.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"><i class="bx bx-plus"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-img"><img src="assets/img/portfolio/jakutes.jpg" class="img-fluid" alt=""></div>
            <div class="portfolio-info">
              <h4>Website</h4>
              <p>Jamu Jakutes</p>
              <a href="assets/img/portfolio/jakutes.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"><i class="bx bx-plus"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-wik">
            <div class="portfolio-img"><img src="assets/img/portfolio/sinom.jpg" class="img-fluid" alt=""></div>
            <div class="portfolio-info">
              <h4>Wikipedia</h4>
              <p>Jamu Sinom</p>
              <a href="assets/img/portfolio/sinom.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link"><i class="bx bx-plus"></i></a>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Section -->