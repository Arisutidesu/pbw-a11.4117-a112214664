<!-- ======= Pricing Section ======= -->
<section id="harga" class="pricing">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Harga</h2>
          <p>Melalui riset, harga disini hanya melalui toko online seperti Blibli, Tokopedia, Shopee diluar toko online saya tidak tahu harga pasti</p>
        </div>

        <div class="row">

          <div class="col-lg-4" data-aos="fade-up" data-aos-delay="100">
            <div class="box">
              <h3>Blibli</h3>
              <h4><sup>IDR</sup>3K-100K<span></span></h4>
              <ul>
                <li><i class="bx bx-check"></i>Harga bervariatif</li>
                <li class="na"><i class="bx bx-x"></i> <span>Beragam jenis jamu</span></li>
                <li class="na"><i class="bx bx-x"></i> <span>Banyak pembeli</span></li>
                <li class="na"><i class="bx bx-x"></i> <span>Banyak yang menjual jamu</span></li>
              </ul>
              <a href="https://www.blibli.com/jual/jamu" class="buy-btn">Dimulai</a>
            </div>
          </div>

          <div class="col-lg-4" data-aos="fade-up" data-aos-delay="100">
            <div class="box">
              <h3>Shopee</h3>
              <h4><sup>IDR</sup>1K-50K<span></span></h4>
              <ul>
                <li><i class="bx bx-check"></i>Harga bervariatif</li>
                <li class="na"><i class="bx bx-x"></i> <span>Beragam jenis jamu</span></li>
                <li class="na"><i class="bx bx-x"></i> <span>Banyak pembeli</span></li>
                <li class="na"><i class="bx bx-x"></i> <span>Banyak yang menjual jamu</span></li>
              </ul>
              <a href="https://shopee.co.id/list/Jamu" class="buy-btn">Dimulai</a>
            </div>
          </div>

          <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
            <div class="box featured">
              <h3>Tokopedia</h3>
              <h4><sup>IDR</sup>5K-90K<span></span></h4>
              <ul>
                <li><i class="bx bx-check"></i>Harga bervariatif</li>
                <li><i class="bx bx-check"></i>Beragam jenis jamu</li>
                <li><i class="bx bx-check"></i>Banyak pembeli</li>
                <li><i class="bx bx-check"></i>Banyak yang menjual jamu</li>
              </ul>
              <a href="https://www.tokopedia.com/find/jamu-tradisional" class="buy-btn">Dimulai</a>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Pricing Section -->