  <!-- ======= Frequently Asked Questions Section ======= -->
  <section id="faq" class="faq section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>FAQ</h2>
          <p>Pertanyaan yang sering terlintas dibenak maupun tidak terlintas dipikiran pembaca seputar tentang jamu</p>
        </div>

        <div class="faq-list">
          <ul>
            <li data-aos="fade-up" data-aos-delay="100">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" class="collapse" data-bs-target="#faq-list-1">Apa perbedaan Jamu, Obat Herbal Terstandar (OHT), dan Fitofarmaka? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-1" class="collapse show" data-bs-parent=".faq-list">
                <p>
                Jamu merupakan Keamanan dan kemanfaatan dibuktikan secara empiris. OHT merupakan Keamanan dan kemanfaatan dibuktikan secara ilmiah melalui uji pra klinik dan Fitofarmaka adalah Keamanan dan kemanfaatan dibuktikan secara uji klinik
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="200">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-2" class="collapsed">Persyaratan mutu apa yang harus dipenuhi untuk produk obat tradisional dan suplemen kesehatan? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-2" class="collapse" data-bs-parent=".faq-list">
                <p>
                Persyaratan mutu lengkap terdapat dalam Peraturan Kepala Badan POM No. 12 Tahun 2014 tentang Persyaratan Mutu Obat Tradisional.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="300">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-3" class="collapsed">Berapa biaya yang diperlukan untuk registrasi produk? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-3" class="collapse" data-bs-parent=".faq-list">
                <p>
                Biaya registrasi sesuai dengan jenis dan jalur registrasi yang terdapat pada Peraturan Pemerintah No. 32 Tahun 2017 tentang Jenis dan Tarif PNBP yang Berlaku Pada Badan POM. Peraturan tersebut dapat didownload di http://jdih.pom.go.id/.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="400">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-4" class="collapsed">Bagaimana cara untuk mendapatkan izin edar produk obat tradisional? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-4" class="collapse" data-bs-parent=".faq-list">
                <p>
                Pelaku Usaha mengajukan permohonan pemeriksaan sarana produksi ke Balai POM di Mamuju untuk mendapatkan Surat Rekomendasi Pemenuhan Aspek CPOTB Bertahap. Kemudian, Balai Besar POM di Mamuju mengajukan penerbitan Surat Keterangan Penerapan Cara Produksi Obat Tradisional yang Baik Bertahap ke Direktorat Pengawasan Obat Tradisional dan Suplemen Kesehatan Badan POM, selanjutnya disampaikan ke Pelaku Usaha. Setelah itu, Pelaku usaha mengajukan permohonan pendaftaran izin edar produk obat tradisonal secara online melalui aplikasi publik https://asrot.pom.go.id
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="500">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-5" class="collapsed">Apa saja yang termasuk kategori produk obat tradisional low risk? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-5" class="collapse" data-bs-parent=".faq-list">
                <p>
                Obat tradisional low risk adalah obat tradisional dengan komposisi sederhana yang hanya mengandung simplisia yang sudah dikenal secara empiris dengan klaim penggunaan tradisional, dengan tingkat pembuktian umum, dalam bentuk sediaan sederhana minyak obat luar, parem, tapel, pilis, rempah mandi, serbuk luar, salep, ratus, serbuk obat dalam, cairan obat dalam dimana profil keamanan dan kemanfaatan telah diketahui pasti.Daftar tanaman yang termasuk dalam bahan low risk adalah : http://asrot.pom.go.id/index.php/home/depan/informasi/85
                </p>
              </div>
            </li>

          </ul>
        </div>

      </div>
    </section><!-- End Frequently Asked Questions Section -->
