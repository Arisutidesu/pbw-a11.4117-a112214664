  <!-- ======= Why Us Section ======= -->
  <section id="kenapa" class="why-us section-bg">
      <div class="container-fluid" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

            <div class="content">
              <h3>Pertanyaan yang <strong>tidak ada</strong> di penjelasan tentang jamu </h3>
              <p>
                Pertanyaan ini kurang lebih sama dengan FAQ tetapi ini bersifat teori yang tidak pernah terpikirkans
              </p>
            </div>

            <div class="accordion-list">
              <ul>
                <li>
                  <a data-bs-toggle="collapse" class="collapse" data-bs-target="#accordion-list-1"><span>01</span> Kenapa jamu tidak boleh ada BKO? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-1" class="collapse show" data-bs-parent=".accordion-list">
                    <p>
                    Jamu yang mengandung BKO sangat membahayakan bagi kesehatan apalagi jika digunakan dalam waktu yang lama. Efek samping yang dapat terjadi antara lain dapat menyebabkan tukak lambung, gagal ginjal dan gangguan hati (liver)
                    </p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2" class="collapsed"><span>02</span> Kenapa disebut jamu? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                    Kata jamu berasal dari bahasa Jawa kuno, yaitu jampi atau usodo. Jampi atau usodo memiliki arti penyembuhan menggunakan ramuan obat-obatan atau doa-doa. Istilah jampi banyak ditemukan pada naskah kuno, seperti pada naskah Gatotkacasraya yang ditulis oleh Mpu Panuluh dari Kerajaan Kediri pada masa Raja Jayabaya
                    </p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3" class="collapsed"><span>03</span> Mengapa obat tradisional tidak diperbolehkan dalam dunia medis? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                    Karena obat tradisional di Indonesia diedarkan secara bebas (merupakan produk OTC) sehingga konsumen dapat menggunakan setiap saat bila dikehendaki. Bila pada obat tradisional terdapat BKO, maka penggunaan yang terus menerus atau berlebihan akan menimbulkan risiko yang membahayakan kesehatan tubuh.
                    </p>
                  </div>
                </li>

              </ul>
            </div>

          </div>

          <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/herb.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
        </div>

      </div>
    </section><!-- End Why Us Section -->