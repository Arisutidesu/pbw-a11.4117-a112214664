 <!-- ======= Team Section ======= -->
 <section id="team" class="team section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Team</h2>
          <p>Website ini tidak bisa dibangun tanpa adanya team yang berkompeten dan bekerjasama dengan baik. Team inilah yang memiliki ide dan gagasan yang baik di bidangnya</p>
        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
              <div class="pic"><img src="assets/img/team/team-1.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Makabe Masamune</h4>
                <span>CEO of Risu</span>
                <p>No pain, no gain</p>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4 mt-lg-0">
            <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="200">
              <div class="pic"><img src="assets/img/team/team-2.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Aki Adagaki</h4>
                <span>Model of Risu</span>
                <p>Bing Chiling</p>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4">
            <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="300">
              <div class="pic"><img src="assets/img/team/team-3.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Raku Ichijou</h4>
                <span>Programmer of Risu</span>
                <p>Let me show my self</p>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4">
            <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="300">
              <div class="pic"><img src="assets/img/team/team-4.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Chitoge Kirisaki</h4>
                <span>Marketing of Risu</span>
                <p>Illness will make it difficult for you to act</p>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Team Section -->
