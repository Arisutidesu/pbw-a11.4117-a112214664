 <!-- ======= Services Section ======= -->
 <section id="pelayanan" class="services section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Pelayanan</h2>
          <p>Kami juga menyediakan beberapa jamu dan juga penjelasan tentang khasiat yang dihasilkan setelah meminum jamu tersebut</p>
        </div>

        <div class="row">
          <div class="col-xl-3 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4><a href="">Jamu Beras Kencur</a></h4>
              <p>Beras kencur adalah campuran dari ekstrak kencur, beras, jahe dan asam jawa. Rasanya manis dan segar. Di dalamnya mengandung tinggi antioksidan yang dapat melindungi sel tubuh dari kerusakan akibat radikal bebas</p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4><a href="">Jamu Kunyit Asam</a></h4>
              <p>Jamumu terbuat dari kunyit, asam jawa, gula merah dan temulawak. Warna kuning pada kunyit asam berasal dari senyawa kurkumin. Senyawa ini mengandung antioksidan yang bersifat antiinflamasi dan antikanker.</p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4><a href="">Jamu Brotowali</a></h4>
              <p>Jamu ini dikenal dengan istilah ‘pahitan’. Bahan bakunya terbuat dari daun sambiloto. Jamu ini bermanfaat untuk mengatasi pegal-pegal, meredakan gatal-gatal, menambah nafsu makan dan mencegah diabetes. Kamu juga bisa mengonsumsi pahitan ini untuk menghilangkan jerawat dan bruntusan di wajah.</p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4><a href="">Jamu Kunci Sirih</a></h4>
              <p>Jamu ini terbuat dari rimpang kunci dan daun sirih. Kunci sirih bermanfaat untuk mengatasi keputihan, merapatkan bagian kewanitaan, memperkuat gigi dan menghilangkan bau badan.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->