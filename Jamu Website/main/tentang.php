 <!-- ======= About Us Section ======= -->
 <section id="tentang" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Tentang Jamu</h2>
        </div>

        <div class="row content">
          <div class="col-lg-6">
            <p>
            Jamu adalah sebutan untuk obat tradisional dari Indonesia. Belakangan populer dengan sebutan herba atau herbal. Jamu dibuat dari bahan-bahan alami, berupa bagian dari tumbuh-tumbuhan seperti rimpang (akar-akaran), daun-daunan, kulit batang, dan buah. Ada juga menggunakan bahan dari tubuh hewan, seperti empedu kambing, empedu ular, atau tangkur buaya. Jamu mempunyai manfaat seperti :
            </p>
            <ul>
              <li><i class="ri-check-double-line"></i>untuk obat</li>
              <li><i class="ri-check-double-line"></i>untuk menjaga kebugaran tubuh dan mencegah dari penyakit</li>
              <li><i class="ri-check-double-line"></i>untuk membantu meningkatkan nafsu makan bagi anak-anak</li>
            </ul>
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <p>
            Jamu biasanya terasa pahit sehingga perlu ditambah madu sebagai pemanis agar rasanya lebih dapat ditoleransi peminumnya. Bahkan ada pula jamu yang ditambah dengan anggur. Selain sebagai pengurang rasa pahit, anggur juga berfungsi untuk menghangatkan tubuh. Untuk info lebih lanjut tentang Jamu dapat dilihat lanjutannya
            </p>
            <a href="https://id.wikipedia.org/wiki/Jamu" class="btn-learn-more">Baca lebih banyak</a>
          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->
