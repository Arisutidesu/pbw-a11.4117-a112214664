<main id="main">
    <!-- ======= Hero Section ======= -->
    <?php include 'main/hero.php';?>
    <!-- End Hero -->

    <!-- ======= About Section ======= -->
    <?php include 'main/tentang.php';?>
    <!-- End About Section -->

    <!-- ======= Services Section ======= -->
    <?php include 'main/pelayanan.php';?>
    <!-- End Services Section -->

    <!-- ======= Portfolio Section ======= -->
    <?php include 'main/gambar.php';?>
    <!-- End Portfolio Section -->

    <!-- ======= Pricing Section ======= -->
    <?php include 'main/harga.php';?>
    <!-- End Pricing Section -->

    <!-- ======= Team Section ======= -->
    <?php include 'main/team.php';?>
    <!-- End Team Section -->

    <!-- ======= Client Section ======= -->
    <?php include 'main/client.php';?>
    <!-- End Team Section -->

    <!-- ======= skill Section ======= -->
    <?php include 'main/keahlian.php';?>
    <!-- End Testimonials Section -->

    <!-- ======= Why Us Section ======= -->
    <?php include 'main/kenapa.php';?>
    <!-- End Why Us Section -->

    <!-- ======= Frequently Asked Questions Section ======= -->
    <?php include 'main/faq.php';?>
    <!-- End Frequently Asked Questions Section -->
    
    <!-- ======= Contact Section ======= -->
    <?php include 'main/kontak.php';?>
    <!-- End Contact Section -->

</main><!-- End #main -->
