 <!-- ======= Footer ======= -->
 <footer id="footer">

<div class="footer-newsletter">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6">
        <h4>Baca di Website Saya</h4>
        <p>Tidak hanya berita seputar tentang kesehatan, topik yang saya hadirkan di website ini bersifat bebas</p>
        <form action="" method="post">
          <input type="email" name="email"><input type="submit" value="Subscribe">
        </form>
      </div>
    </div>
  </div>
</div>

<div class="footer-top">
  <div class="container">
    <div class="row">

      <div class="col-lg-3 col-md-6 footer-contact">
        <h3>Risu</h3>
        <p>
          Arisu<br>
          Semarang, Jawa Tengah<br>
          Indonesia <br><br>
          <strong>Email :</strong> aristidesbima@gmail.com<br>
        </p>
      </div>

      <div class="col-lg-3 col-md-6 footer-links">
        <h4>Link jika dibutuhkan</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="#hero">Beranda</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#tentang">Tentang Jamu</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#pelayanan">Pelayanan</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#gambar">Gambar Jamu</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#harga">Harga</a></li>
        </ul>
      </div>

      <div class="col-lg-3 col-md-6 footer-links">
        <h4>Link jika dibutuhkan</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="#team">Team</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#keahlian">Keahlian</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#kenapa">Kenapa?</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#faq">FAQ</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#kontak">Kontak</a></li>
        </ul>
      </div>

      <div class="col-lg-3 col-md-6 footer-links">
        <h4>Sosial Media</h4>
        <p>Jika ada hal yang ingin ditanyakan, jangan sungkan kontak saya melalui sosial media dibawah ini</p>
        <div class="social-links mt-3">
          <a href="https://twitter.com/Arisukun6" class="twitter"><i class="bx bxl-twitter"></i></a>
          <a href="https://instagram.com/arisukunx" class="instagram"><i class="bx bxl-instagram"></i></a>
          <a href="https://wa.me/6283842825162" class="whatsapp"><i class="bx bxl-whatsapp"></i></a>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="container footer-bottom clearfix">
  <div class="copyright">
    &copy; Copyright <strong><span>Risu</span></strong>. All Rights Reserved
  </div>
  <div class="credits">
    <!-- All the links in the footer should remain intact. -->
    <!-- You can delete the links only if you purchased the pro version. -->
    <!-- Licensing information: https://bootstrapmade.com/license/ -->
    <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/ -->
  </div>
</div>
</footer><!-- End Footer -->


<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="assets/vendor/aos/aos.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>

</body>